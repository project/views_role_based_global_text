<?php

namespace Drupal\views_role_based_global_text;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\RoleInterface;
use Drupal\views\Plugin\views\area\Text;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Views area text handler overwriting class to provide role based access.
 */
class RoleBasedGlobalText extends Text {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Constructs a RoleBasedGlobalText object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current_user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountInterface $current_user,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['roles_fieldset']['default'] = FALSE;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['roles_fieldset'] = [
      '#type'  => 'details',
      '#title' => $this->t('Roles'),
    ];

    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $roles = array_map(fn(RoleInterface $role) => Html::escape($role->label()), $roles);
    $form['roles_fieldset']['roles'] = [
      '#title' => $this->t('Select Roles'),
      '#type' => 'checkboxes',
      '#options' => $roles,
      '#default_value' => $this->options['roles_fieldset']['roles'] ?? [],
      '#description' => $this->t('Only the checked roles will be able to access this value. If no role is selected, available to all.'),
    ];

    $form['roles_fieldset']['negate'] = [
      '#title' => $this->t('Negate'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['roles_fieldset']['negate'] ?? FALSE,
      '#description' => $this->t('Exclude the selected roles from accessing this value. If no role is selected, available to all.'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($empty = FALSE) {
    // Get the checked roles.
    $checked_roles = $this->options['roles_fieldset'] && is_array($this->options['roles_fieldset']['roles']) ? array_filter($this->options['roles_fieldset']['roles']) : [];
    $is_negated = $this->options['roles_fieldset']['negate'] ?? FALSE;

    // Roles assigned to logged-in users.
    $user_roles = $this->currentUser->getRoles();

    // If no role is selected, show to all users.
    if (empty($checked_roles)) {
      return parent::render($empty);
    }

    // If roles selected but not negated, show only to the selected roles.
    if (array_intersect($user_roles, $checked_roles) && !$is_negated) {
      return parent::render($empty);
    }

    // If roles selected and also negated, show to all roles exclude selected.
    if (!array_intersect($user_roles, $checked_roles) && $is_negated) {
      return parent::render($empty);
    }

    return [];
  }

}
