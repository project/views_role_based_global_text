<?php

namespace Drupal\Tests\views_role_based_global_text\Functional;

use Drupal\views\Views;

/**
 * View access tests class for Views Field Permissions browser tests.
 */
class ViewsRoleBasedGlobalTextViewAccessTest extends ViewsRoleBasedGlobalTextTestBase {

  /**
   * Tests access with no roles selected.
   */
  public function testNoRolesSelected() {
    $view = Views::getView('views_role_based_global_text_test');
    $view->setDisplay();
    $view->initHandlers();
    $view->setHandlerOption('page_1', 'header', 'area', 'roles_fieldset', [
      'roles' => [],
      'negate' => 0,
    ]);
    $view->save();

    // Test as authenticated or anonymous user, both should have access.
    $this->assertTrue(\Drupal::currentUser()->isAuthenticated(), 'Current user is authenticated.');
    $this->drupalGet('/views-role-based-global-text-test');
    $this->assertSession()->pageTextContains('test header content');
    $this->drupalLogout();
    $this->drupalGet('/views-role-based-global-text-test');
    $this->assertFalse(\Drupal::currentUser()->isAuthenticated(), 'Current user is not authenticated.');
    $this->assertSession()->pageTextContains('test header content');
  }

  /**
   * Tests roles access based on configuration.
   */
  public function testRolesSelected() {
    $view = Views::getView('views_role_based_global_text_test');
    $view->setDisplay();
    $view->initHandlers();
    $view->setHandlerOption('page_1', 'header', 'area', 'roles_fieldset', [
      'roles' => [
        'authenticated' => 'authenticated',
        'anonymous' => 0,
      ],
      'negate' => 0,
    ]);
    $view->save();

    // Test as authenticated user.
    $this->assertTrue(\Drupal::currentUser()->isAuthenticated(), 'Current user is authenticated.');
    $this->drupalGet('/views-role-based-global-text-test');
    $this->assertSession()->pageTextContains('test header content');

    // Test as anonymous user with no authenticated user role.
    $this->drupalLogout();
    $this->drupalGet('/views-role-based-global-text-test');
    $this->assertFalse(\Drupal::currentUser()->isAuthenticated(), 'Current user is not authenticated.');
    $this->assertSession()->pageTextNotContains('test header content');
  }

  /**
   * Tests the negate configuration option.
   */
  public function testViewAccessNegateOptionSelected() {
    $view = Views::getView('views_role_based_global_text_test');
    $view->setDisplay();
    $view->initHandlers();
    $view->setHandlerOption('page_1', 'header', 'area', 'roles_fieldset', [
      'roles' => [
        'authenticated' => 'authenticated',
      ],
      'negate' => 1,
    ]);
    $view->save();

    // Test as authenticated user.
    $this->assertTrue(\Drupal::currentUser()->isAuthenticated(), 'Current user is authenticated.');
    $this->drupalGet('/views-role-based-global-text-test');
    $this->assertSession()->pageTextNotContains('test header content');

    // Test as anonymous user with no authenticated user role.
    $this->drupalLogout();
    $this->drupalGet('/views-role-based-global-text-test');
    $this->assertFalse(\Drupal::currentUser()->isAuthenticated(), 'Current user is not authenticated.');
    $this->assertSession()->pageTextContains('test header content');
  }

}
