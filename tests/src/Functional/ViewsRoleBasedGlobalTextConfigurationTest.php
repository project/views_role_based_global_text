<?php

namespace Drupal\Tests\views_role_based_global_text\Functional;

use Drupal\views\Views;

/**
 * Configuration tests class for Views Field Permissions browser tests.
 */
class ViewsRoleBasedGlobalTextConfigurationTest extends ViewsRoleBasedGlobalTextTestBase {

  /**
   * Tests the "Views Field Permissions" in field handler edit form.
   */
  public function testConfigurationSubmitting() {
    $this->drupalGet('/admin/structure/views/nojs/handler/views_role_based_global_text_test/page_1/header/area');
    $this->submitForm(
      [
        'options[roles_fieldset][roles][authenticated]' => TRUE,
        'options[roles_fieldset][roles][anonymous]' => FALSE,
        'options[roles_fieldset][negate]' => TRUE,
      ],
      'Apply'
    );
    $this->drupalGet('admin/structure/views/view/views_role_based_global_text_test');
    $this->submitForm([], 'Save');

    // Load view and init handlers.
    $view = Views::getView('views_role_based_global_text_test');
    $view->setDisplay();
    $view->initHandlers();

    // Check if header area options are saved.
    $handler = $view->display_handler->getHandler('header', 'area');
    $this->assertTrue(in_array('authenticated', $handler->options['roles_fieldset']['roles']), 'Control option saved successfully.');
    $this->assertFalse(in_array('anonymous', $handler->options['roles_fieldset']['roles']), 'Control option saved successfully.');
    $this->assertEquals(1, $handler->options['roles_fieldset']['negate'], 'Negate option saved successfully.');
  }

}
