<?php

namespace Drupal\Tests\views_role_based_global_text\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Base class for Views Role Based Global Text browser tests.
 */
abstract class ViewsRoleBasedGlobalTextTestBase extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'views_ui',
    'views_role_based_global_text_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create admin user.
    $admin_user = $this->drupalCreateUser(['administer site configuration', 'administer views']);
    $this->drupalLogin($admin_user);
  }

}
